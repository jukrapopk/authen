package com.jukrapopk.authen.repository;

import com.jukrapopk.authen.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {}