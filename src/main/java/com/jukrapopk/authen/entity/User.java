package com.jukrapopk.authen.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@JsonPropertyOrder({"user_id", "name"})
@Table(name = "user")
@Entity
public class User {
    @JsonProperty("user_id")
    private @Id String userId;
    private String name;

    public User() {}

    public User(String user_id, String name) {
        this.userId = user_id;
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String user_id) {
        this.userId = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //For testing / logging
    @Override
    public String toString() {
        return "id=" + this.userId + ", name=" + this.name;
    }
}