package com.jukrapopk.authen.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegisterUserResponseDto {
    @JsonProperty("user_id")
    private String userId;

    public RegisterUserResponseDto() {}

    public RegisterUserResponseDto(String user_id) {
        this.userId = user_id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
