package com.jukrapopk.authen.dto;

public class RegisterUserRequestDto {
    private String name;

    public RegisterUserRequestDto() {}

    public RegisterUserRequestDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
