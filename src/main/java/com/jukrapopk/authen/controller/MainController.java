package com.jukrapopk.authen.controller;

import com.jukrapopk.authen.dto.ErrorResponseDto;
import com.jukrapopk.authen.dto.GetUserRequestDto;
import com.jukrapopk.authen.dto.RegisterUserRequestDto;
import com.jukrapopk.authen.dto.RegisterUserResponseDto;
import com.jukrapopk.authen.entity.User;
import com.jukrapopk.authen.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class MainController {
    enum ErrorCode {
        UserNotExist(1001),
        BlankName(1002);

        public final int code;

        ErrorCode(int code) {
            this.code = code;
        }
    }

    private final UserRepository repository;

    MainController(UserRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/users")
    private List<User> getAllUsers() {
        return repository.findAll();
    }

    @GetMapping("/get_user")
    private ResponseEntity<?> getUser(@RequestBody GetUserRequestDto request) {
        Optional<User> user = repository.findById(request.getUserId());
        if (!user.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(new ErrorResponseDto("User does not exist.", ErrorCode.UserNotExist.code));
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(user.get());
        }
    }

    @PostMapping("/register_user")
    private ResponseEntity<?> registerUser(@RequestBody RegisterUserRequestDto request) {
        if (request.getName().isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(new ErrorResponseDto("Name is blank", ErrorCode.BlankName.code));
        } else {
            User tempUser = new User(UUID.randomUUID().toString(), request.getName());
            repository.save(tempUser);
            return ResponseEntity.status(HttpStatus.CREATED).body(new RegisterUserResponseDto(tempUser.getUserId()));
        }
    }
}
