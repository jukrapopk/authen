package com.jukrapopk.authen.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jukrapopk.authen.dto.ErrorResponseDto;
import com.jukrapopk.authen.dto.RegisterUserResponseDto;
import com.jukrapopk.authen.entity.User;
import com.jukrapopk.authen.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.BDDAssumptions.given;
import static org.junit.jupiter.api.Assertions.*;

@AutoConfigureMockMvc
@SpringBootTest
class MainControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper mapper;

    private final String uuidRegex = "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}";

    @Test
    public void testRegisterUser() throws Exception {
        String requestBody = "{\"name\": \"PlayerNameXY\"}";
        MockHttpServletResponse response = mvc.perform(post("/register_user").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON).content(requestBody)).andReturn().getResponse();
        RegisterUserResponseDto responseContent = mapper.readValue(response.getContentAsString(), RegisterUserResponseDto.class);

        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
        assertTrue(responseContent.getUserId().matches(uuidRegex)); // Check if pattern matches UUID
    }


    @Test
    public void testGetUser() throws Exception {
        //Mock up data
        String used_id = "00000000-0000-0000-0000-000000000000";
        String name = "PlayerNameXY";
        User user = new User(used_id, name);
        when(userRepository.findById(used_id)).thenReturn(Optional.of(user));

        //User exist
        String requestBody1 = "{\"user_id\": \"00000000-0000-0000-0000-000000000000\"}";
        MockHttpServletResponse response1 = mvc.perform(get("/get_user").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON).content(requestBody1)).andReturn().getResponse();
        User responseContent1 = mapper.readValue(response1.getContentAsString(), User.class);

        assertThat(response1.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(responseContent1.getUserId()).isEqualTo(used_id);
        assertThat(responseContent1.getName()).isEqualTo(name);

        //User doesn't exist
        String requestBody2 = "{\"user_id\": \"00000000-1111-0000-1111-000000000000\"}";
        MockHttpServletResponse response2 = mvc.perform(get("/get_user").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON).content(requestBody2)).andReturn().getResponse();
        ErrorResponseDto responseContent2 = mapper.readValue(response2.getContentAsString(), ErrorResponseDto.class);

        assertThat(response2.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(responseContent2.getMessage()).isEqualTo("User does not exist.");
        assertThat(responseContent2.getCode()).isEqualTo(1001);
    }

}